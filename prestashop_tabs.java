import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;




public class prestashop_tabs {

    private static String BASE_URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";

    public static void main(String[] args) {

                String property = System.getProperty("user.dir") + "/Driver/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);


        String email = "webinar.test@gmail.com";
        String password = "Xcg7299bnSmMuRLp9ITw";
        String presta_check = "prestashop-automation";
        String logout_text = "ВХОД";


        WebDriver driver = new ChromeDriver();


        driver.manage().window().maximize();
        driver.get(BASE_URL);
//Login
        WebElement login = driver.findElement(By.id("email"));
        login.sendKeys(email);

        WebElement passwrd = driver.findElement(By.id("passwd"));
        passwrd.sendKeys(password);

        WebElement submit = driver.findElement(By.className("ladda-label"));
        submit.click();

        sleep(1000);

//Checking that user logged in
        String presteCheck = driver.findElement(By.id("header_shopname")).getText();
        System.out.println(presteCheck.contains(presta_check));

        // opening tabs

        int tabsQty = driver.findElements(By.className("maintab")).size();
        String tabHeader = new String();

        for (int i = 0; i < tabsQty; i++) {

            if (i == 3 | i == 7) {
                WebElement currentTab = driver.findElements(By.className("link-levelone")).get(i);
                currentTab.click();
            } else {
                WebElement currentTab = driver.findElements(By.className("maintab")).get(i);
                currentTab.click();
            }
            sleep(1000);
            if (i == 2 | i == 6)
                tabHeader = driver.findElement(By.cssSelector(".header-toolbar h2")).getText();
            else
                tabHeader = driver.findElement(By.className("page-title")).getText();

            String url = driver.getCurrentUrl();
            System.out.println(tabHeader);
            driver.navigate().refresh();

            sleep(1000);

            // checking that we are on the same page
            System.out.println(url.contains(driver.getCurrentUrl()));
        }

        driver.quit();
    }


    public static void  sleep (long ms) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

