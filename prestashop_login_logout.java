import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;



public class prestashop_login_logout {

    private static String BASE_URL="http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";

    public static void main(String[] args) {



         String property = System.getProperty("user.dir") + "/Driver/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);


        String email = "webinar.test@gmail.com";
        String password="Xcg7299bnSmMuRLp9ITw";
        String presta_check="prestashop-automation";
        String logout_text="ВХОД";


       WebDriver driver = new ChromeDriver();



        driver.manage().window().maximize();
        driver.get(BASE_URL);
//Login
        WebElement login=driver.findElement(By.id("email"));
        login.sendKeys(email);

        WebElement passwrd=driver.findElement(By.id("passwd"));
        passwrd.sendKeys(password);

        WebElement submit=driver.findElement(By.className("ladda-label"));
        submit.click();

        sleep(1000);

//Checking that user logged in
        String presteCheck=driver.findElement(By.id("header_shopname")).getText();
        System.out.println(presteCheck.contains(presta_check));

//Logout

        WebElement userIcon=driver.findElement(By.className("employee_avatar_small"));
        userIcon.click();

        WebElement logOut=driver.findElement(By.id("header_logout"));
        logOut.click();
        sleep(1000);

//Checking that user logged out

        String logoutCheck=driver.findElement(By.className("ladda-label")).getText();
        System.out.println(logoutCheck.contains(logout_text));


        driver.quit();


    }

    public static void  sleep (long ms) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
